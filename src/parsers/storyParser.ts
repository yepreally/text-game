import { ILineType, IScene, IStory } from '../types';
import { parseOption } from './optionParser';
import { parseScene } from './sceneParser';
import { parseScript } from './scriptParser';

const isLineType = (lineType: ILineType, line: string): boolean => {
  return line.trim().startsWith(`${lineType}:`);
};

const removeTrailingEmptyLines = (script: string[]): string[] => {
  const { arr } = script.reverse().reduce<{ arr: string[], foundNonEmptyLine: boolean }>((acc, line) => {
    if (acc.foundNonEmptyLine || line.length > 0) {
      acc.foundNonEmptyLine = true;
      acc.arr.push(line);
    }

    return acc;
  }, { arr: [], foundNonEmptyLine: false });

  return arr.reverse();
};

export const parseStory = (storyText: string): IStory => {
  const { scenes } = storyText.split('\n').reduce<{ scenes: IScene[], lastLineType: ILineType | null }>((acc, rawLine) => {
    const line = rawLine.trim();

    if (isLineType('scene', line)) {
      acc.lastLineType = 'scene';
      acc.scenes.push(parseScene(line));

      return acc;
    }

    if (isLineType('option', line)) {
      acc.lastLineType = 'option';
      acc.scenes[acc.scenes.length - 1].options.push(parseOption(line));
      return acc;
    }

    if (isLineType('script', line) || acc.lastLineType === 'script') {
      acc.lastLineType = 'script';
      const script = parseScript(line);
      acc.scenes[acc.scenes.length - 1].script.push(script);
      return acc;
    }

    return acc;
  }, { scenes: [], lastLineType: null });

  return scenes.reduce<IStory>((acc, scene) => {
    acc[scene.id] = {
      ...scene,
      script: removeTrailingEmptyLines(scene.script),
    };
    return acc;
  }, {});
};
