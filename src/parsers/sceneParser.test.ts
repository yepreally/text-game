import { parseScene } from './sceneParser';

describe.each([
  ['scene:scene id', 'scene id'],
  ['scene:scene id ', 'scene id'],
  ['scene: scene id', 'scene id'],
  ['scene: scene id ', 'scene id'],
  ['scene:   scene id   ', 'scene id'],
  ['scene:scene id=>', 'scene id'],
  ['scene:scene id =>', 'scene id'],
  ['scene: scene id=>', 'scene id'],
  ['scene: scene id =>', 'scene id'],
  ['scene:   scene id   =>', 'scene id'],
])('parseScene with', (line, expectedId) => {
  test(`"${line}" should result in "${expectedId}" id`, () => {
    expect(parseScene(line).id).toEqual(expectedId);
  });
});

describe.each([
  ['scene: scene id', undefined],
  ['scene: scene id => ', undefined],
  ['scene: scene id => foo', undefined],
  ['scene: scene id => state=1', undefined],
  ['scene: scene id => foo=1,start', undefined],
  ['scene: scene id => foo=1,end', undefined],
  ['scene: scene id => start', 'start'],
  ['scene: scene id => state=start', 'start'],
  ['scene: scene id=> start', 'start'],
  ['scene: scene id=> state=start', 'start'],
  ['scene: scene id =>start', 'start'],
  ['scene: scene id =>state=start', 'start'],
  ['scene: scene id=>start', 'start'],
  ['scene: scene id=>state=start', 'start'],
  ['scene: scene id => end', 'end'],
  ['scene: scene id => state=end', 'end'],
  ['scene: scene id=> end', 'end'],
  ['scene: scene id=> state=end', 'end'],
  ['scene: scene id =>end', 'end'],
  ['scene: scene id =>state=end', 'end'],
  ['scene: scene id=>end', 'end'],
  ['scene: scene id=>state=end', 'end'],
  ['scene: scene id => foo=1,state=start', 'start'],
  ['scene: scene id=> foo=1,state=start', 'start'],
  ['scene: scene id =>foo=1,state=start', 'start'],
  ['scene: scene id=>foo=1,state=start', 'start'],
  ['scene: scene id => foo=1,state=end', 'end'],
  ['scene: scene id=> foo=1,state=end', 'end'],
  ['scene: scene id =>foo=1,state=end', 'end'],
  ['scene: scene id=>foo=1,state=end', 'end'],
])('parseScene with', (line, expectedState) => {
  test(`"${line}" should result in "${expectedState}" state`, () => {
    expect(parseScene(line).state).toEqual(expectedState);
  });
});

describe.each([
  ['scene: scene id', []],
  ['scene: scene id => start', []],
  ['scene: scene id => end', []],
  ['scene: scene id => foo', []],
  ['scene: scene id => state=start', []],
  ['scene: scene id => state=end', []],
  ['scene: scene id => state=1', []],
  ['scene: scene id =>foo=1,bar,baz=+2,a=-10,b=/20,c=*999,d=a', [
    { key: 'foo', operation: '=', value: 1 },
    { key: 'baz', operation: '+', value: 2 },
    { key: 'a', operation: '-', value: 10 },
    { key: 'b', operation: '/', value: 20 },
    { key: 'c', operation: '*', value: 999 },
  ]],
  ['scene: scene id => foo = 1 , bar , baz = +2 , a = -10 , b = /20 , c = *999 , d = a', [
    { key: 'foo', operation: '=', value: 1 },
    { key: 'baz', operation: '+', value: 2 },
    { key: 'a', operation: '-', value: 10 },
    { key: 'b', operation: '/', value: 20 },
    { key: 'c', operation: '*', value: 999 },
  ]],
])('parseScene with', (line, expectedEffects) => {
  test(`"${line}" should result in "${JSON.stringify(expectedEffects)}" effects`, () => {
    expect(parseScene(line).effects).toEqual(expectedEffects);
  });
});
