import { IEffect, IScene, IState } from '../types';

interface ISceneRegexGroups {
  id: string;
  effects?: string;
}

interface IEffectRegexGroups {
  operation: '+' | '-' | '*' | '/';
  value: string;
}

const STATE_KEY = 'state';
const START = 'start';
const END = 'end';
const UNKNOWN = 'unknown';

export const parseState = (key: string, value: string, idx: number): typeof START | typeof END | typeof UNKNOWN | null => {
  if ((key === START || key === END) && !value && idx === 0) {
    return key;
  }

  if (key === STATE_KEY && (value === START || value === END)) {
    return value;
  }

  if (key === STATE_KEY) {
    return UNKNOWN;
  }

  return null;
};

export const parseEffect = (key: string, value: string): IEffect | null => {
  if (key && value) {
    const match = value.match(/^(?<operation>[+-\/\*]?)\s*(?<value>\d*)$/) as unknown as { groups: IEffectRegexGroups };
    const valueInt = parseInt(match?.groups.value, 10);
    const operation = match?.groups.operation || '=';

    if (!isNaN(valueInt)) {
      return { key, operation, value: valueInt };
    }
  }

  return null;
};

export const parseEffects = (rawEffects?: string): { effects: IEffect[], state?: IState } => {
  return (rawEffects ?? '').split(',').reduce<{ effects: IEffect[], state?: IState }>((acc, rawEffect, idx) => {
    const [untrimmedKey, untrimmedValue] = rawEffect.split('=');
    const key = untrimmedKey.trim();
    const value = (untrimmedValue ?? '').trim();

    const state = parseState(key, value, idx);
    if (state) {
      if (state !== UNKNOWN) {
        acc.state = state;
      }
      return acc;
    }

    const effect = parseEffect(key, value);
    if (effect) {
      acc.effects.push(effect);
      return acc;
    }

    return acc;
  }, { effects: [] });
};

export const parseScene = (line: string): IScene => {
  const { groups } = line.match(/^scene:\s*(?<id>((?!=>).)*)\s*((=>)?(?<effects>.*))?$/) as unknown as { groups: ISceneRegexGroups };

  const { effects, state } = parseEffects(groups.effects);

  return {
    id: groups.id.trim(),
    state,
    script: [],
    options: [],
    effects,
  };
};
