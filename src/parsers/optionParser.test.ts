import { parseOption } from './optionParser';

describe.each([
  ['option:option label', 'option label'],
  ['option:option label ', 'option label'],
  ['option: option label', 'option label'],
  ['option: option label ', 'option label'],
  ['option:   option label   ', 'option label'],
  ['option:option label=>', 'option label'],
  ['option:option label =>', 'option label'],
  ['option: option label=>', 'option label'],
  ['option: option label =>', 'option label'],
  ['option:   option label   =>', 'option label'],
])('parseOption with', (line, expectedLabel) => {
  test(`"${line}" should result in "${expectedLabel}" label`, () => {
    expect(parseOption(line).label).toEqual(expectedLabel);
  });
});

describe.each([
  ['option: option label', null],
  ['option: option label => ', null],
  ['option: option label => foo=1,goto id', null],
  ['option: option label => foo=1,end', null],
  ['option: option label => goto id', 'goto id'],
  ['option: option label => goto=goto id', 'goto id'],
  ['option: option label=> goto id', 'goto id'],
  ['option: option label=> goto=goto id', 'goto id'],
  ['option: option label =>goto id', 'goto id'],
  ['option: option label =>goto=goto id', 'goto id'],
  ['option: option label=>goto id', 'goto id'],
  ['option: option label=>goto=goto id', 'goto id'],
  ['option: option label => foo=1,goto=goto id', 'goto id'],
  ['option: option label=> foo=1,goto=goto id', 'goto id'],
  ['option: option label =>foo=1,goto=goto id', 'goto id'],
  ['option: option label=>foo=1,goto=goto id', 'goto id'],
])('parseOption with', (line, expectedGoto) => {
  test(`"${line}" should result in "${expectedGoto}" goto`, () => {
    expect(parseOption(line).goto).toEqual(expectedGoto);
  });
});

describe.each([
  ['option: option label', []],
  ['option: option label => start', []],
  ['option: option label => end', []],
  ['option: option label => foo', []],
  ['option: option label => goto=goto label', []],
  ['option: option label =>foo=1,bar,baz=+2,a=-10,b=/20,c=*999,d=a', [
    { key: 'foo', operation: '=', value: 1 },
    { key: 'baz', operation: '+', value: 2 },
    { key: 'a', operation: '-', value: 10 },
    { key: 'b', operation: '/', value: 20 },
    { key: 'c', operation: '*', value: 999 },
  ]],
  ['option: option label => foo = 1 , bar , baz = +2 , a = -10 , b = /20 , c = *999 , d = a', [
    { key: 'foo', operation: '=', value: 1 },
    { key: 'baz', operation: '+', value: 2 },
    { key: 'a', operation: '-', value: 10 },
    { key: 'b', operation: '/', value: 20 },
    { key: 'c', operation: '*', value: 999 },
  ]],
])('parseOption with', (line, expectedEffects) => {
  test(`"${line}" should result in "${JSON.stringify(expectedEffects)}" effects`, () => {
    expect(parseOption(line).effects).toEqual(expectedEffects);
  });
});
