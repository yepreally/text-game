import { parseStory } from './storyParser';

test('parseStory', () => {
  const storyText = `
    scene: intro => start
    script: This is the start of a story.

    You are the main character.

    What is your name?

    option: C3P0 => introduceC3P0
    option: R2D2 => introduceR2D2

    scene: introduceC3P0 => end
    script: Hello C3P0!

    Good-bye!

    scene: introduceR2D2 => end
    script: Hello R2!

  `;

  expect(parseStory(storyText)).toEqual({
    intro: {
      effects: [],
      id: 'intro',
      options: [
        {
          effects: [],
          goto: 'introduceC3P0',
          label: 'C3P0',
        },
        {
          effects: [],
          goto: 'introduceR2D2',
          label: 'R2D2',
        },
      ],
      script: [
        'This is the start of a story.',
        '',
        'You are the main character.',
        '',
        'What is your name?',
      ],
      state: 'start',
    },
    introduceC3P0: {
      effects: [],
      id: 'introduceC3P0',
      options: [],
      script: [
        'Hello C3P0!',
        '',
        'Good-bye!',
      ],
      state: 'end',
    },
    introduceR2D2: {
      effects: [],
      id: 'introduceR2D2',
      options: [],
      script: [
        'Hello R2!',
      ],
      state: 'end',
    },
  });
});
