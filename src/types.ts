export type IOperation = '=' | '+' | '-' | '*' | '/';

export interface IEffect {
  key: string;
  operation: IOperation;
  value: number;
}

export type ILineType = 'scene' | 'script' | 'option';

export type IState = 'start' | 'end';

export interface IOption {
  label: string;
  goto: string | null;
  effects: IEffect[];
}

export interface IScene {
  id: string;
  script: string[];
  effects: IEffect[];
  options: IOption[];
  state?: IState;
}

export interface IStory {
  [key: string]: IScene;
}
